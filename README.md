# Answer:
### AnswerCreateDTO:
private Integer studentId;->(not null)  
private Integer taskItemId;->(not null)  
private String answerResourceUrl;->(not null)
### AnswerFullDTO:
private Integer id;->(not null)  
private Integer studentId;->(not null)  
private Integer taskItemId;->(not null)  
private Instant created;->(not null)
private String answerResourceUrl;->(not null)   
private Integer realGrade;  
private String teacherComment;
###AnswerUpdateDTO:
private Integer id;->(not null)  
private String answerResourceUrl;->(not null)   
private Integer realGrade;  
private String teacherComment
***

# TaskItem:
### TaskItemCreateDTO:
private String taskResourceUrl;->(not null)   
private Integer maximumGrade;    
private Integer homeworkId;->(not null)
### TaskItemFullDTO:
private Integer id;->(not null)  
private String taskResourceUrl;->(not null)   
private Integer maximumGrade;    
private Integer homeworkId;->(not null)  
private List&lt;AnswerFullDto> answers;   
### TaskItemUpdateDTO:
private Integer id;->(not null)  
private String taskResourceUrl;->(not null)   
private Integer maximumGrade;    
***

# Homework:
### HomeworkCreateDTO:
private Integer teacherId;->(not null)  
private Instant deadLine;->(not null)  
private Integer groupId;->(not null)  
private String topic;->(not null)   
private List&lt;TaskItemCreateDto> tasks;
### HomeworkFullDTO:
private Integer id;->(not null)  
private Integer teacherId;->(not null)  
private Instant created;->(not null)  
private Instant deadLine;->(not null)  
private Integer groupId;->(not null)  
private String topic;->(not null)   
private List&lt;TaskItemFullDto> tasks;
### HomeworkPreviewDTO:
private Integer id;->(not null)  
private Integer teacherId;->(not null)  
private Instant created;->(not null)  
private Instant deadLine;->(not null)  
private Integer groupId;->(not null)  
private String topic;->(not null)
### HomeworkUpdateDTO:
private Integer id;->(not null)  
private Instant deadLine;->(not null)  
private Integer groupId;->(not null)  
private String topic;->(not null)   

