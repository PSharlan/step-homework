package by.itstep.stephomework.repository;

import by.itstep.stephomework.entity.AnswerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<AnswerEntity, Integer> {

    Page<AnswerEntity> findAllByStudentId(Integer studentId, Pageable pageable);

    List<AnswerEntity> findAllByHomeworkId(Integer homeworkId);

    List<AnswerEntity> findByStudentIdAndHomeworkId(Integer studentId, Integer homeworkId);


}
