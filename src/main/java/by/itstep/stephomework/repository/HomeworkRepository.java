package by.itstep.stephomework.repository;

import by.itstep.stephomework.entity.HomeworkEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomeworkRepository extends JpaRepository<HomeworkEntity, Integer> {

    Page<HomeworkEntity> findAllByGroupId(Integer groupId, Pageable pageable);

    Page<HomeworkEntity> findAllByTeacherId(Integer teacherId, Pageable pageable);

}
