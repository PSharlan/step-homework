package by.itstep.stephomework.dto.homework;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class HomeworkPreviewDto {

    @ApiModelProperty(example = "1", notes = "ID for this homework")
    private Integer id;

    @ApiModelProperty(example = "1", notes = "ID of the teacher who created the homework")
    private Integer teacherId;

    @ApiModelProperty(example = "2021-01-20T16:16:50Z", notes = "When the homework is created")
    private Instant created;

    @ApiModelProperty(example = "2021-02-20T16:16:50Z", notes = "homework deadline")
    private Instant deadLine;

    @ApiModelProperty(example = "1", notes = "Group ID for which homework")
    private Integer groupId;

    @ApiModelProperty(example = "the topic of this homework", notes = "The topic of this homework")
    private String topic;

    @ApiModelProperty(example = "the description of this homework", notes = "The description of this homework")
    private String description;

    @ApiModelProperty(example = "http://somecloud.com/task-url", notes = "URL or path to the file with the task")
    private String taskResourceUrl;

    @ApiModelProperty(example = "2021-02-20T16:16:50Z", notes = "lesson date for homework")
    private Instant lessonDate;

}
