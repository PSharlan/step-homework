package by.itstep.stephomework.dto.homework;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class HomeworkCreateDto {

    @NotNull(message = "teacher ID cannot be empty, only existing teacher ID")
    @ApiModelProperty(example = "1", notes = "ID of the teacher who created the homework")
    private Integer teacherId;

    @NotNull(message = "deadline cannot be empty")
    @ApiModelProperty(example = "2021-02-20T16:16:50Z", notes = "homework deadline")
    private Instant deadLine;

    @NotNull(message = "group ID cannot be empty, only existing group ID")
    @ApiModelProperty(example = "1", notes = "Group ID for which homework")
    private Integer groupId;

    @NotBlank(message = "the topic cannot be empty")
    @ApiModelProperty(example = "the topic of this homework", notes = "The topic of this homework")
    private String topic;

    @NotBlank(message = "the description cannot be empty")
    @ApiModelProperty(example = "the description of this homework", notes = "The description of this homework")
    private String description;

    @NotBlank(message = "the task cannot be empty")
    @ApiModelProperty(example = "http://somecloud.com/task-url", notes = "URL or path to the file with the task")
    private String taskResourceUrl;

    @NotNull(message = "date of lesson cannot be empty")
    @ApiModelProperty(example = "2021-02-20T16:16:50Z", notes = "lesson date for homework")
    private Instant lessonDate;

}
