package by.itstep.stephomework.dto.answer;

import by.itstep.stephomework.entity.enums.AnswerStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class AnswerFullDto {

    @ApiModelProperty(example = "1", notes = "ID for this answer")
    private Integer id;

    @ApiModelProperty(example = "1", notes = "Student ID of the author of the answer")
    private Integer studentId;

    @ApiModelProperty(example = "1", notes = "ID of the homework to which the answer belongs")
    private Integer homeworkId;

    @ApiModelProperty(example = "2021-01-20T16:16:50Z", notes = "When the answer is created")
    private Instant created;

    @ApiModelProperty(example = "http://somecloud.com/student-url",
            notes = "URL or path to the file with the answer")
    private String answerResourceUrl;

    @ApiModelProperty(example = "5", notes = "Teacher's real grade for this answer")
    private Integer realGrade;

    @ApiModelProperty(example = "some comment from teacher", notes = "Teacher`s comment for this answer")
    private String teacherComment;

    @ApiModelProperty(example = "ASSESSED", notes = "Answer status, can take one of three values:" +
            "PENDING or ASSESSED or RETAKE")
    private AnswerStatus answerStatus;

    @ApiModelProperty(example = "some comment from student, if it's necessary",
            notes = "some comment from student for answer")
    private String studentComment;

}
