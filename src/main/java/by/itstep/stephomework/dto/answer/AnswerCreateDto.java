package by.itstep.stephomework.dto.answer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AnswerCreateDto {

    @NotNull(message = "student ID cannot be empty, only existing student ID")
    @ApiModelProperty(example = "1", notes = "Student ID of the author of the answer")
    private Integer studentId;

    @NotNull(message = "homework ID cannot be empty, only existing taskItem ID")
    @ApiModelProperty(example = "1", notes = "ID of the homework to which the answer belongs")
    private Integer homeworkId;

    @NotBlank(message = "the answer cannot be empty")
    @ApiModelProperty(example = "http://somecloud.com/student-url",
            notes = "URL or path to the file with the answer")
    private String answerResourceUrl;

    @ApiModelProperty(example = "some comment from student, if it's necessary",
            notes = "some comment from student for answer")
    private String studentComment;

}
