package by.itstep.stephomework.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "homework")
public class HomeworkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "teacher_id", nullable = false)
    private Integer teacherId;

    @Column(name= "created", nullable = false)
    private Instant created;

    @Column(name= "dead_line")
    private Instant deadLine;

    @Column(name= "group_id", nullable = false)
    private Integer groupId;

    @Column(name= "topic", nullable = false)
    private String topic;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "task_resource_url", nullable = false)
    private String taskResourceUrl;

    @Column(name= "lesson_date", nullable = false)
    private Instant lessonDate;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "homework", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<AnswerEntity> answers = new ArrayList<>();

}
