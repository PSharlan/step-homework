package by.itstep.stephomework.entity.enums;

public enum AnswerStatus {
    PENDING,
    ASSESSED,
    RETAKE
}
