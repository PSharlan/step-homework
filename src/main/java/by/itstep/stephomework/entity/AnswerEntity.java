package by.itstep.stephomework.entity;

import by.itstep.stephomework.entity.enums.AnswerStatus;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "student_answer")
public class AnswerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "student_id", nullable = false)
    private Integer studentId;

    @ManyToOne
    @JoinColumn(name = "homework_id")
    private HomeworkEntity homework;

    @Column(name = "created", nullable = false)
    private Instant created;

    @Column(name = "answer_resource_url", nullable = false)
    private String answerResourceUrl;

    @Column(name = "real_grade")
    private Integer realGrade;

    @Column(name = "comment")
    private String teacherComment;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", nullable = false)
    private AnswerStatus answerStatus;

    @Column(name = "student_comment")
    private String studentComment;

}
