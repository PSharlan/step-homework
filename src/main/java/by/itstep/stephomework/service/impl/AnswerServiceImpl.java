package by.itstep.stephomework.service.impl;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.entity.enums.AnswerStatus;
import by.itstep.stephomework.exception.*;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import by.itstep.stephomework.service.AnswerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.mapper.AnswerMapper.ANSWER_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;

    private final HomeworkRepository homeworkRepository;

    @Override
    @Transactional(readOnly = true)
    public AnswerFullDto findById(Integer id) {
        AnswerFullDto foundAnswerFullDto = answerRepository.findById(id)
                .map(ANSWER_MAPPER::mapToFullDto)
                .orElseThrow(()-> new AnswerNotFoundException(id));

        log.info("AnswerServiceImpl -> Answer {} was found", foundAnswerFullDto);

        return foundAnswerFullDto;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AnswerFullDto> findAllByStudentId(Integer studentId, Pageable pageable) {
        Page<AnswerFullDto> foundAnswerList = answerRepository.findAllByStudentId(studentId, pageable)
                .map(ANSWER_MAPPER::mapToFullDto);

        log.info("AnswerServiceImpl -> Answers {} was found", foundAnswerList.getTotalElements());

        return foundAnswerList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AnswerFullDto> findByStudentIdAndHomeworkId(Integer studentId, Integer homeworkId) {
        List<AnswerFullDto> foundAnswers= ANSWER_MAPPER.mapToFullDtoList(
                answerRepository.findByStudentIdAndHomeworkId(studentId, homeworkId));

        log.info("AnswerServiceImpl -> Answer by student {} was found", foundAnswers.size());

        return foundAnswers;
    }

    @Override
    @Transactional
    public AnswerFullDto create(AnswerCreateDto answerCreateDto) {
        AnswerEntity answerEntityToSave = ANSWER_MAPPER.mapToEntity(answerCreateDto);
        answerEntityToSave.setAnswerStatus(AnswerStatus.PENDING);

        HomeworkEntity homework = homeworkRepository
                .findById(answerCreateDto.getHomeworkId())
                .orElseThrow(() -> new HomeworkNotFoundException(answerCreateDto.getHomeworkId()));

        answerEntityToSave.setHomework(homework);

        answerEntityToSave.setCreated(Instant.now().truncatedTo(ChronoUnit.SECONDS));
        createdCorrectTimeChecked(answerEntityToSave);
        exceptionIfAnswerExist(answerEntityToSave);

        AnswerEntity savedAnswerEntity = answerRepository.save(answerEntityToSave);

        log.info("AnswerServiceImpl -> Answer {} was created", savedAnswerEntity);

        return ANSWER_MAPPER.mapToFullDto(savedAnswerEntity);
    }

    @Override
    @Transactional
    public AnswerFullDto update(AnswerUpdateDto answerUpdateDto) {
        AnswerEntity answerEntityToUpdate = answerRepository.findById(answerUpdateDto.getId())
                .orElseThrow(() -> new AnswerNotFoundException(answerUpdateDto.getId()));

        checkAndSetAllUpdatesToEntity(answerEntityToUpdate, answerUpdateDto);

        AnswerEntity updatedAnswerEntity = answerRepository.save(answerEntityToUpdate);

        log.info("AnswerServiceImpl -> Answer {} was updated", updatedAnswerEntity);

        return ANSWER_MAPPER.mapToFullDto(updatedAnswerEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        AnswerEntity answerEntityToDelete = answerRepository.findById(id)
                .orElseThrow(() -> new AnswerNotFoundException(id));

        exceptionIfThereIsRatingOrComment(answerEntityToDelete);

        answerRepository.deleteById(id);

        log.info("AnswerServiceImpl -> Answer by id {} was deleted", id);
    }

    private void exceptionIfThereIsRatingOrComment(AnswerEntity answerEntity){
        if(!answerEntity.getAnswerStatus().equals(AnswerStatus.PENDING)){
            throw new AnswerHasRatingOrCommentException("Answer " + answerEntity.getId() +
                    " has a grade and ASSESSED");
        }
    }

    private void createdCorrectTimeChecked(AnswerEntity answerEntity){
        Integer homeworkId = answerEntity.getHomework().getId();
        HomeworkEntity homeworkEntity = homeworkRepository.findById(homeworkId)
                .orElseThrow(() -> new HomeworkNotFoundException(homeworkId));

        if(answerEntity.getCreated().isAfter(homeworkEntity.getDeadLine())){
            throw new AnswerTooLateException(answerEntity.getCreated(), homeworkEntity.getDeadLine());
        }
    }

    private AnswerEntity checkAndSetAllUpdatesToEntity(AnswerEntity entity, AnswerUpdateDto updateDto){

        if(!entity.getAnswerResourceUrl().equals(updateDto.getAnswerResourceUrl())
                && entity.getRealGrade() != null){

            throw new AnswerHasRatingOrCommentException("Answer " + entity.getId() + " has a grade");

        }
        entity.setAnswerResourceUrl(updateDto.getAnswerResourceUrl());
        entity.setRealGrade(updateDto.getRealGrade());
        entity.setTeacherComment(updateDto.getTeacherComment());
        setStatusIfChange(entity, updateDto);
        return entity;
    }

    private AnswerEntity setStatusIfChange(AnswerEntity entity, AnswerUpdateDto updateDto){
        if(!updateDto.getAnswerStatus().equals(entity.getAnswerStatus())){
            if(updateDto.getRealGrade() == null
                    && !entity.getAnswerStatus().equals(AnswerStatus.PENDING)) {
                throw new AnswerHasNoRating(updateDto.getId());
            }else{
                entity.setAnswerStatus(updateDto.getAnswerStatus());
            }
        }
        return entity;
    }

    private void exceptionIfAnswerExist(AnswerEntity checkedAnswerEntity){
        boolean isPresent = answerRepository.findByStudentIdAndHomeworkId(checkedAnswerEntity.getStudentId(),
                checkedAnswerEntity.getHomework().getId()).isEmpty();
        if(!isPresent) {
            List<AnswerEntity> answerEntities = answerRepository.findByStudentIdAndHomeworkId(checkedAnswerEntity.getStudentId(),
                    checkedAnswerEntity.getHomework().getId());
            for (AnswerEntity entity : answerEntities) {
                if (!entity.getAnswerStatus().equals(AnswerStatus.RETAKE)) {
                    throw new AnswerAlreadyExist(checkedAnswerEntity.getStudentId(), checkedAnswerEntity.getHomework().getId());

                }
            }
        }
    }

}
