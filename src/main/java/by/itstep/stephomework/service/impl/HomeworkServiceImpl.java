package by.itstep.stephomework.service.impl;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.exception.HomeworkHasAnswerException;
import by.itstep.stephomework.exception.HomeworkNotFoundException;
import by.itstep.stephomework.exception.WrongDeadLineException;
import by.itstep.stephomework.exception.WrongLessonDateException;
import by.itstep.stephomework.mapper.HomeworkWithAnswerMapper;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import by.itstep.stephomework.service.HomeworkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.mapper.HomeworkMapper.HOMEWORK_MAPPER;


@Slf4j
@Service
public class HomeworkServiceImpl implements HomeworkService {

    private final HomeworkRepository homeworkRepository;

    private final AnswerRepository answerRepository;

    private final HomeworkWithAnswerMapper homeworkWithAnswerMapper;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepository homeworkRepository, AnswerRepository answerRepository, HomeworkWithAnswerMapper homeworkWithAnswerMapper) {
        this.homeworkRepository = homeworkRepository;
        this.answerRepository = answerRepository;
        this.homeworkWithAnswerMapper = homeworkWithAnswerMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public HomeworkFullDto findById(Integer id) {
        HomeworkFullDto foundHomework = homeworkRepository.findById(id)
                .map(HOMEWORK_MAPPER::mapToFullDto)
                .orElseThrow(() -> new HomeworkNotFoundException(id));

        log.info("HomeworkServiceImpl -> Homework by id {} was found", foundHomework.getId());

        return foundHomework;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HomeworkPreviewDto> findByGroupId(Integer groupId, Pageable pageable) {
        Page<HomeworkPreviewDto> foundHomeworks = homeworkRepository.findAllByGroupId(groupId,
                pageable)
                .map(HOMEWORK_MAPPER::mapToPreviewDto);

        log.info("HomeworkServiceImpl -> Homeworks {} was found", foundHomeworks.getTotalElements());

        return foundHomeworks;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HomeworkFullDto> findByGroupIdWithAnswers(Integer groupId, Pageable pageable) {

        Page<HomeworkEntity> foundHomeworksEntity = homeworkRepository.findAllByGroupId(groupId, pageable);
        foundHomeworksEntity.forEach(homeworkEntity -> homeworkEntity.setAnswers(
                answerRepository.findAllByHomeworkId(homeworkEntity.getId())));

        Page<HomeworkFullDto>foundHomeworks=foundHomeworksEntity.map(HOMEWORK_MAPPER::mapToFullDto);

        log.info("HomeworkServiceImpl -> Homeworks {} was found", foundHomeworks.getTotalElements());

        return foundHomeworks;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HomeworkWithAnswerPreviewDTO> findByGroupIdWithAnswersByStudentId(Integer groupId, Integer studentId,
                                                                                  Pageable pageable) {

        Page<HomeworkWithAnswerPreviewDTO> foundHomeworks = homeworkRepository
                .findAllByGroupId(groupId, pageable)
                .map(homeworkEntity ->
                        homeworkWithAnswerMapper.mapToPreview(homeworkEntity, studentId));

        log.info("HomeworkServiceImpl -> Homeworks {} was found", foundHomeworks.getTotalElements());

        return foundHomeworks;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HomeworkPreviewDto> findByTeacherId(Integer teacherId, Pageable pageable) {
        Page<HomeworkPreviewDto> foundHomeworks = homeworkRepository.findAllByTeacherId(teacherId,
                pageable)
                .map(HOMEWORK_MAPPER::mapToPreviewDto);

        log.info("HomeworkServiceImpl -> Homeworks by Teacher {} was found",
                foundHomeworks.getTotalElements());

        return foundHomeworks;
    }

    @Override
    @Transactional
    public HomeworkFullDto create(HomeworkCreateDto homeworkCreateDto) {
        HomeworkEntity homeworkEntityToSave = HOMEWORK_MAPPER.mapToEntity(homeworkCreateDto);

        homeworkEntityToSave.setCreated(Instant.now().truncatedTo(ChronoUnit.SECONDS));
        exceptionIfDeadLineBeforeCreat(homeworkEntityToSave);
        HomeworkEntity savedHomeworkEntity = homeworkRepository.save(homeworkEntityToSave);

        log.info("HomeworkServiceImpl -> Homework {} was created", savedHomeworkEntity.getId());

        return HOMEWORK_MAPPER.mapToFullDto(savedHomeworkEntity);
    }

    @Override
    @Transactional
    public HomeworkFullDto update(HomeworkUpdateDto homeworkUpdateDto) {
        HomeworkEntity homeworkEntityToUpdate = homeworkRepository
                .findById(homeworkUpdateDto.getId())
                .orElseThrow(() -> new HomeworkNotFoundException(homeworkUpdateDto.getId()));

        exceptionIfLessonDateAfterDeadLineOrCreated(homeworkEntityToUpdate,homeworkUpdateDto);
        homeworkEntityToUpdate.setDeadLine(homeworkUpdateDto.getDeadLine());
        homeworkEntityToUpdate.setLessonDate(homeworkUpdateDto.getLessonDate());
        homeworkEntityToUpdate.setGroupId(homeworkUpdateDto.getGroupId());
        homeworkEntityToUpdate.setTopic(homeworkUpdateDto.getTopic());
        homeworkEntityToUpdate.setDescription(homeworkUpdateDto.getDescription());
        if(!homeworkEntityToUpdate.getTaskResourceUrl().equals(homeworkUpdateDto.getTaskResourceUrl())){
            List<AnswerEntity> answers = homeworkEntityToUpdate.getAnswers();
            exceptionIfThereIsAnAnswer(answers);
            homeworkEntityToUpdate.setTaskResourceUrl(homeworkUpdateDto.getTaskResourceUrl());
        }

        HomeworkEntity updatedHomeworkEntity = homeworkRepository.save(homeworkEntityToUpdate);

        log.info("HomeworkServiceImpl -> Homework {} was updated", updatedHomeworkEntity.getId());

        return HOMEWORK_MAPPER.mapToFullDto(updatedHomeworkEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        HomeworkEntity homeworkEntityToDelete = homeworkRepository.findById(id)
                .orElseThrow(() -> new HomeworkNotFoundException(id));

        List<AnswerEntity> answers = homeworkEntityToDelete.getAnswers();
        exceptionIfThereIsAnAnswer(answers);

        homeworkRepository.deleteById(id);

        log.info("HomeworkServiceImpl -> Homework by id {} was deleted", id);
    }

    private void exceptionIfThereIsAnAnswer(List<AnswerEntity> answers){
            if (!answers.isEmpty()) {
                throw new HomeworkHasAnswerException();
            }
    }

    private void exceptionIfDeadLineBeforeCreat(HomeworkEntity homeworkEntity){
        if(homeworkEntity.getDeadLine().isBefore(homeworkEntity.getCreated())
           || homeworkEntity.getDeadLine().isBefore(homeworkEntity.getLessonDate())){
            throw new WrongDeadLineException();
        }
    }

    private void exceptionIfLessonDateAfterDeadLineOrCreated(HomeworkEntity homeworkEntity,
                                                             HomeworkUpdateDto homeworkUpdateDto){
        if(homeworkUpdateDto.getLessonDate().isAfter(homeworkEntity.getCreated())
           || homeworkUpdateDto.getLessonDate().isAfter(homeworkUpdateDto.getDeadLine())){
            throw new WrongLessonDateException();
        }
    }

}
