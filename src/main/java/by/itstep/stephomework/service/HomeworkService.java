package by.itstep.stephomework.service;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HomeworkService {

    HomeworkFullDto findById(Integer id);

    Page<HomeworkPreviewDto> findByGroupId(Integer groupId, Pageable pageable);

    Page<HomeworkFullDto> findByGroupIdWithAnswers(Integer groupId, Pageable pageable);

    Page<HomeworkWithAnswerPreviewDTO> findByGroupIdWithAnswersByStudentId(Integer groupId, Integer studentId,
                                                                           Pageable pageable);

    Page<HomeworkPreviewDto> findByTeacherId(Integer teacherId, Pageable pageable);

    HomeworkFullDto create(HomeworkCreateDto homeworkCreateDto);

    HomeworkFullDto update(HomeworkUpdateDto homeworkUpdateDto);

    void deleteById(Integer id);

}
