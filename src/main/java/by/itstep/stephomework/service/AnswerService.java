package by.itstep.stephomework.service;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AnswerService {

    AnswerFullDto findById(Integer id);

    Page<AnswerFullDto> findAllByStudentId(Integer studentId, Pageable pageable);

    List<AnswerFullDto> findByStudentIdAndHomeworkId(Integer studentId, Integer homeworkId);

    AnswerFullDto create(AnswerCreateDto answerCreateDto);

    AnswerFullDto update(AnswerUpdateDto answerUpdateDto);

    void deleteById(Integer id);

}
