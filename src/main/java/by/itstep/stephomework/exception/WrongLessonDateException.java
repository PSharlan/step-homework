package by.itstep.stephomework.exception;

public class WrongLessonDateException extends WrongDateException{

    private static String message = "Lesson date must be before created date and dead line";

    public WrongLessonDateException(){
        super(message);
    }

}
