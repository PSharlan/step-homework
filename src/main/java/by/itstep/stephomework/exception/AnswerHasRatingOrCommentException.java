package by.itstep.stephomework.exception;

public class AnswerHasRatingOrCommentException extends RuntimeException{

    public AnswerHasRatingOrCommentException(String message ){
        super(message);
    }

}
