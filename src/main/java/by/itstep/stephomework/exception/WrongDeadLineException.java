package by.itstep.stephomework.exception;

public class WrongDeadLineException extends WrongDateException{

    private static String message = "Dead Line must be after created date, or lesson date";

    public WrongDeadLineException() {
        super(message);
    }

}
