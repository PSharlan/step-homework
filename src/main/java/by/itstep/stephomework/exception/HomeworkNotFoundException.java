package by.itstep.stephomework.exception;

public class HomeworkNotFoundException extends EntityNotFoundException{

    public HomeworkNotFoundException(Integer id){
        super("Homework was not found by id: " + id);
    }

}
