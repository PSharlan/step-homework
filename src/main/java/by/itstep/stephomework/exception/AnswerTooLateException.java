package by.itstep.stephomework.exception;

import java.time.Instant;

public class AnswerTooLateException extends RuntimeException{

    public AnswerTooLateException(Instant created, Instant deadLine){
        super("Too late. Because you create at " + created + " after " + deadLine);
    }

}
