package by.itstep.stephomework.exception;

public class AnswerAlreadyExist extends RuntimeException{

    public AnswerAlreadyExist(Integer studentId, Integer homeworkId){
        super("Answer from student with id:" + studentId + " for homework with id:" + homeworkId);
    }

}
