package by.itstep.stephomework.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class Handler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handle (EntityNotFoundException exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AnswerHasRatingOrCommentException.class)
    public ResponseEntity<Object> handle (AnswerHasRatingOrCommentException exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AnswerTooLateException.class)
    public ResponseEntity<Object> handle (AnswerTooLateException exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AnswerHasNoRating.class)
    public ResponseEntity<Object> handle (AnswerHasNoRating exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HomeworkHasAnswerException.class)
    public ResponseEntity<Object> handle (HomeworkHasAnswerException exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AnswerAlreadyExist.class)
    public ResponseEntity<Object> handle (AnswerAlreadyExist exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WrongDateException.class)
    public ResponseEntity<Object> handle (WrongDateException exception){
        log.info(exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
