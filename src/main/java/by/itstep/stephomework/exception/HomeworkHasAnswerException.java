package by.itstep.stephomework.exception;

public class HomeworkHasAnswerException extends RuntimeException{

    public HomeworkHasAnswerException(){
        super("Homework has answer(s)!");
    }

}
