package by.itstep.stephomework.exception;

public class AnswerHasNoRating extends RuntimeException{
    public AnswerHasNoRating(Integer answerId){
        super("The answer " + answerId + " no has rating for change status");
    }
}
