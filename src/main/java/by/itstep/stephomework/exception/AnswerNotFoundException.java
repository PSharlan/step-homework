package by.itstep.stephomework.exception;

public class AnswerNotFoundException extends EntityNotFoundException{

    public AnswerNotFoundException(Integer id){
        super("Answer was not found by id: " + id);
    }

}
