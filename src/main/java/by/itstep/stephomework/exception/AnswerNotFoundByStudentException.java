package by.itstep.stephomework.exception;

public class AnswerNotFoundByStudentException extends EntityNotFoundException{

    public AnswerNotFoundByStudentException(Integer studentId, Integer homeworkId){
        super("Answer was not found by student id: " + studentId + " or homework id: " + homeworkId);
    }

}
