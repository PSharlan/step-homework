package by.itstep.stephomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepHomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepHomeworkApplication.class, args);
	}

}
