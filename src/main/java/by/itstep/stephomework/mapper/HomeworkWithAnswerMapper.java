package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.entity.HomeworkEntity;

public interface HomeworkWithAnswerMapper {

    HomeworkWithAnswerPreviewDTO mapToPreview(HomeworkEntity homeworkEntity, Integer studentId);

}
