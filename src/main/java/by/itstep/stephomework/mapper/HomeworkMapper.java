package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.entity.HomeworkEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = AnswerMapper.class)
public interface HomeworkMapper {

    HomeworkMapper HOMEWORK_MAPPER = Mappers.getMapper(HomeworkMapper.class);

    HomeworkPreviewDto mapToPreviewDto(HomeworkEntity homeworkEntity);

    List<HomeworkPreviewDto> mapToPreviewDtoList(List<HomeworkEntity> homeworkEntities);

    HomeworkFullDto mapToFullDto(HomeworkEntity homeworkEntity);

    @Mapping(target = "id", ignore = true)
    HomeworkEntity mapToEntity(HomeworkCreateDto homeworkCreateDto);

    @Mapping(target = "answers", ignore = true )
    HomeworkEntity mapToEntity(HomeworkUpdateDto homeworkUpdateDto);

}
