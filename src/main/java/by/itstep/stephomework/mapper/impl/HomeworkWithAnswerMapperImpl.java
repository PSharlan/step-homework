package by.itstep.stephomework.mapper.impl;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.mapper.HomeworkWithAnswerMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class HomeworkWithAnswerMapperImpl implements HomeworkWithAnswerMapper {

    @Override
    public HomeworkWithAnswerPreviewDTO mapToPreview(HomeworkEntity homeworkEntity,
                                                     Integer studentId) {
        if ( homeworkEntity == null ) {
            return null;
        }
        HomeworkWithAnswerPreviewDTO previewDTO = new HomeworkWithAnswerPreviewDTO();
        previewDTO.setId( homeworkEntity.getId() );
        previewDTO.setTeacherId( homeworkEntity.getTeacherId() );
        previewDTO.setCreated( homeworkEntity.getCreated() );
        previewDTO.setDeadLine( homeworkEntity.getDeadLine() );
        previewDTO.setGroupId( homeworkEntity.getGroupId() );
        previewDTO.setTopic( homeworkEntity.getTopic() );
        previewDTO.setDescription( homeworkEntity.getDescription() );
        previewDTO.setTaskResourceUrl( homeworkEntity.getTaskResourceUrl() );
        previewDTO.setLessonDate( homeworkEntity.getLessonDate() );
        if(!homeworkEntity.getAnswers().isEmpty()){
            List<AnswerEntity> studentAnswers = answerByStudentId(homeworkEntity.getAnswers(), studentId);
            if(!studentAnswers.isEmpty()) {
                AnswerEntity lastAnswer = studentAnswers.get(studentAnswers.size()-1);
                previewDTO.setRealGrade(lastAnswer.getRealGrade());
                previewDTO.setAnswerStatus(lastAnswer.getAnswerStatus());
            }
        }
        return previewDTO;
    }

    private List<AnswerEntity> answerByStudentId(List<AnswerEntity> fullAnswers, Integer studentId){
        List<AnswerEntity> studentAnswers = new ArrayList<>();
        for(AnswerEntity entity : fullAnswers){
            if(entity.getStudentId().equals(studentId)){
                studentAnswers.add(entity);
            }
        }
        studentAnswers.sort(Comparator.comparing(AnswerEntity::getCreated));
        return studentAnswers;
    }

}
