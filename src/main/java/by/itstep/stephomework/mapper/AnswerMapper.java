package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AnswerMapper {

    AnswerMapper ANSWER_MAPPER = Mappers.getMapper(AnswerMapper.class);

    @Mapping(target = "homeworkId", source = "homework.id")
    AnswerFullDto mapToFullDto(AnswerEntity answerEntity);

    List<AnswerFullDto> mapToFullDtoList(List<AnswerEntity> answerEntities);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "homework.id", source = "homeworkId")
    AnswerEntity mapToEntity(AnswerCreateDto answerCreateDto);

    AnswerEntity mapToEntity(AnswerUpdateDto answerUpdateDto);

}
