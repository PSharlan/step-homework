package by.itstep.stephomework.controller;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.service.HomeworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "Controller dedicated to manage homeworks")
public class HomeworkController {

    private final HomeworkService homeworkService;

    @GetMapping("/homeworks/{id}")
    @ApiOperation(value = "Find one answer by id", notes = "Existing id must be specified")
    public HomeworkFullDto getById(@PathVariable Integer id){
        return homeworkService.findById(id);
    }

    @GetMapping("/students/groups/{groupId}/homeworks")
    @ApiOperation(value = "Find all homeworks by group ID", notes = "Existing group ID must be specified")
    public Page<HomeworkPreviewDto> getByGroupId(@PathVariable Integer groupId,
                                                 @RequestParam(required = false) Integer page,
                                                 @RequestParam(required = false) Integer size){
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        return homeworkService.findByGroupId(groupId, pageRequest);
    }

    @GetMapping("/teachers/groups/{groupId}/homeworks")
    @ApiOperation(value = "Find all homeworks by group ID who have answers", notes = "Existing group ID must be specified")
    public Page<HomeworkFullDto> getByGroupIdWithAnswers(@PathVariable Integer groupId,
                                                 @RequestParam(required = false) Integer page,
                                                 @RequestParam(required = false) Integer size){
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        return homeworkService.findByGroupIdWithAnswers(groupId, pageRequest);
    }

    @GetMapping("/groups/{groupId}/students/{studentId}/homeworks")
    @ApiOperation(value = "Find all homeworks by group ID and have answers by student ID",
            notes = "Existing group ID must be specified")
    public Page<HomeworkWithAnswerPreviewDTO> getByGroupIdWithAnswersByStudentId(@PathVariable Integer groupId,
                                                                      @PathVariable Integer studentId,
                                                                      @RequestParam(required = false) Integer page,
                                                                      @RequestParam(required = false) Integer size){
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        return homeworkService.findByGroupIdWithAnswersByStudentId(groupId, studentId, pageRequest);
    }

    @GetMapping("/teachers/{teacherId}/homeworks")
    @ApiOperation(value = "Find all homeworks by teacher ID", notes = "Existing teacher ID must be specified")
    public Page<HomeworkPreviewDto> getByTeacherId(@PathVariable Integer teacherId,
                                                   @RequestParam(required = false) Integer page,
                                                   @RequestParam(required = false) Integer size){
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        return homeworkService.findByTeacherId(teacherId, pageRequest);
    }

    @PostMapping("/homeworks")
    @ApiOperation(value = "Create homework",
            notes = "If the homework created without task, then we get an exception")
    public ResponseEntity<HomeworkFullDto> create(@Valid @RequestBody HomeworkCreateDto createDto){
         return new ResponseEntity<>(homeworkService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping("/homeworks")
    @ApiOperation(value = "Update homework", notes = "Homework must be exist (created first)," +
            " else we get an exception")
    public ResponseEntity<HomeworkFullDto> update(@Valid @RequestBody HomeworkUpdateDto updateDto){
        return new ResponseEntity<>(homeworkService.update(updateDto), HttpStatus.OK);
    }

    @DeleteMapping("/homeworks/{id}")
    @ApiOperation(value = "Delete homework by ID",
            notes = "If the homework has a task, then we get an exception")
    public void delete(@PathVariable Integer id){
        homeworkService.deleteById(id);
    }

}
