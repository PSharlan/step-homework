package by.itstep.stephomework.controller;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.service.AnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Api(description = "Conntroller dedicated to manage homework answers")
public class AnswerController {

    private final AnswerService answerService;

    @GetMapping("/answers/{id}")
    @ApiOperation(value = "Find one answer by ID", notes = "Existing ID must be specified")
    public AnswerFullDto getById(@PathVariable Integer id){
        return answerService.findById(id);
    }

    @GetMapping("/students/{studentId}/answers")
    @ApiOperation(value = "Find all answer by student ID", notes = "Existing student ID must be specified")
    public Page<AnswerFullDto> getAllByStudentId(@PathVariable Integer studentId,
                                                 @RequestParam(required = false) Integer page,
                                                 @RequestParam(required = false) Integer size){
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        return answerService.findAllByStudentId(studentId, pageRequest);
    }

    @GetMapping("/answers")
    @ApiOperation(value = "Find one answer by student ID and homework ID",
            notes = "Existing ID`s must be specified")
    public List<AnswerFullDto> getByStudentAndHomeworkId(@RequestParam Integer studentId,
                                                         @RequestParam Integer homeworkId){
        return answerService.findByStudentIdAndHomeworkId(studentId, homeworkId);
    }

    @PostMapping("/answers")
    @ApiOperation(value = "Create answer",
            notes = "If the answer created after deadline, then we get an exception")
    public ResponseEntity<AnswerFullDto> create(@Valid @RequestBody AnswerCreateDto createDto){
        return new ResponseEntity<>(answerService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping("/answers")
    @ApiOperation(value = "Update answer", notes = "Answer must be exist (created first)," +
            " else we get an exception")
    public ResponseEntity<AnswerFullDto> update(@Valid @RequestBody AnswerUpdateDto updateDto){
        return new ResponseEntity<>(answerService.update(updateDto), HttpStatus.OK);
    }

    @DeleteMapping("/answers/{id}")
    @ApiOperation(value = "Delete answer by ID",
            notes = "If the answer has a comment or a teacher's grade, then we get an exception")
    public void delete(@PathVariable Integer id){
        answerService.deleteById(id);
    }

}
