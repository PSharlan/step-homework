package by.itstep.stephomework.repository;

import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class AnswerRepositoryTest {

    @Autowired
    HomeworkRepository homeworkRepository;

    @Autowired
    AnswerRepository answerRepository;

    @BeforeEach
    void setUp() {
        answerRepository.deleteAll();
        homeworkRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity toSave = generateAnswerEntity(grandParentHomeworkEntity);
        AnswerEntity saved = answerRepository.save(toSave);
        //when
        AnswerEntity found = answerRepository.findById(saved.getId()).get();
        //then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved, found);
    }

    @Test
    void findAllByStudentId_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        Integer studentId = 2;
            List<AnswerEntity> answersListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
            AnswerEntity setStudentAnswerEntity = answersListToSave.get(1);
            setStudentAnswerEntity.setStudentId(studentId);
            answersListToSave.set(1, setStudentAnswerEntity);
            answerRepository.saveAll(answersListToSave);

        //when
        Pageable pageable = Pageable.unpaged();
        Page<AnswerEntity> foundList = answerRepository.findAllByStudentId(2, pageable);
        //then
        assertNotNull(foundList);
        for (AnswerEntity found : foundList) {
            assertEquals(studentId, found.getStudentId());
        }
    }

    @Test
    void findAllByHomeworkId_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        answerRepository.saveAll(generateListAnswerEntity(grandParentHomeworkEntity));
        Integer searchHomeworkId = grandParentHomeworkEntity.getId();
        //when
        List<AnswerEntity> foundList = answerRepository.findAllByHomeworkId(searchHomeworkId);
        //then
        assertNotNull(foundList);
        for (AnswerEntity found : foundList) {
            assertEquals(searchHomeworkId, found.getHomework().getId());
        }
    }

//    @Test
//    void findByStudentIdAndTaskItemsId_happyPath() {
//        //given
//        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
//        Integer studentId = 2;
//
//            List<AnswerEntity> answersListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
//            AnswerEntity setStudentEntity = answersListToSave.get(1);
//            setStudentEntity.setStudentId(studentId);
//            answersListToSave.set(1, setStudentEntity);
//            answerRepository.saveAll(answersListToSave);
//
//        //when&then
//
//            AnswerEntity found =
//                    answerRepository.findByStudentIdAndTaskItemId(studentId, parentTaskItemEntity.getId()).get();
//            assertNotNull(found);
//            assertEquals(studentId, found.getStudentId());
//            assertEquals(parentTaskItemEntity.getId(), found.getTaskItem().getId());
//
//    }

    @Test
    void created_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity toSave = generateAnswerEntity(grandParentHomeworkEntity);
        //when
        AnswerEntity created = answerRepository.save(toSave);
        //then
        assertNotNull(created);
        assertNotNull(created.getId());
    }

    @Test
    void update_happyPath() {
        //given
        String newValueAnswerUrl = "new test value";
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity toUpdate = answerRepository.save(generateAnswerEntity(grandParentHomeworkEntity));
        String oldValueAnswerUrl = toUpdate.getAnswerResourceUrl();
        toUpdate.setAnswerResourceUrl(newValueAnswerUrl);
        //when
        AnswerEntity updated = answerRepository.save(toUpdate);
        //then
        assertNotNull(updated);
        assertEquals(toUpdate.getId(), updated.getId());
        assertNotEquals(oldValueAnswerUrl, updated.getAnswerResourceUrl());
        assertEquals(newValueAnswerUrl, updated.getAnswerResourceUrl());
    }

    @Test
    void deleteById() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity toDelete = answerRepository.save(generateAnswerEntity(grandParentHomeworkEntity));
        //when
        answerRepository.deleteById(toDelete.getId());
        //then
        assertFalse(answerRepository.findById(toDelete.getId()).isPresent());
    }

}
