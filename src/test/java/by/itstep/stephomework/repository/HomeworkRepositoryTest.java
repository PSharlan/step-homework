package by.itstep.stephomework.repository;

import by.itstep.stephomework.entity.HomeworkEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class HomeworkRepositoryTest {

    @Autowired
    HomeworkRepository homeworkRepository;

    @Autowired
    AnswerRepository answerRepository;

    @BeforeEach
    void setUp() {
        homeworkRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        HomeworkEntity toSave = generateHomeworkEntity();
        HomeworkEntity saved = homeworkRepository.save(toSave);
        Integer searchId = saved.getId();
        //when
        HomeworkEntity found = homeworkRepository.findById(searchId).get();
        //then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved, found);
    }

    @Test
    void created_happyPath() {
        //given
        HomeworkEntity toSave = generateHomeworkEntity();
        //when
        HomeworkEntity created = homeworkRepository.save(toSave);
        //then
        assertNotNull(created);
        assertNotNull(created.getId());
    }

    @Test
    void update_happyPath() {
        //given
        HomeworkEntity toSave = generateHomeworkEntity();
        HomeworkEntity saved = homeworkRepository.save(toSave);
        String changeValue = "test topic changed";
        HomeworkEntity toUpdate = homeworkRepository.findById(saved.getId()).get();
        toUpdate.setTopic(changeValue);
        //when
        homeworkRepository.save(toUpdate);
        HomeworkEntity updated = homeworkRepository.findById(toUpdate.getId()).get();
        //then
        assertNotNull(updated);
        assertEquals(saved.getId(), updated.getId());
        assertNotEquals(saved.getTopic(), updated.getTopic());
        assertEquals(changeValue, updated.getTopic());
    }

    @Test
    void findAllByGroupId_happyPath() {
        //given
        List<HomeworkEntity> entityListToSave = generateListHomeworkEntity();
        for (HomeworkEntity entity : entityListToSave) {
            entity.setGroupId(3);
        }
        List<HomeworkEntity> savedEntityList = homeworkRepository.saveAll(entityListToSave);
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkEntity> foundEntityPages = homeworkRepository.findAllByGroupId(3, pageable);
        //then
        assertNotNull(foundEntityPages);
        assertEquals(savedEntityList.size(), foundEntityPages.getTotalElements());
        List<HomeworkEntity> foundEntityList = foundEntityPages.toList();
        assertEquals(savedEntityList, foundEntityList);
    }

    @Test
    void findAllByTeacherId_happyPath() {
        //given
        List<HomeworkEntity> entityListToSave = generateListHomeworkEntity();
        for (HomeworkEntity entity : entityListToSave) {
            entity.setTeacherId(1);
        }
        List<HomeworkEntity> savedEntityList = homeworkRepository.saveAll(entityListToSave);
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkEntity> foundEntityPages = homeworkRepository.findAllByTeacherId(1, pageable);
        //then
        assertNotNull(foundEntityPages);
        assertEquals(savedEntityList.size(), foundEntityPages.getTotalElements());
        List<HomeworkEntity> foundEntityList = foundEntityPages.toList();
        assertEquals(savedEntityList, foundEntityList);
    }

    @Test
    void deleteById_happyPath() {
        //given
        HomeworkEntity toDelete = homeworkRepository.save(generateHomeworkEntity());
        //when
        homeworkRepository.deleteById(toDelete.getId());
        //then
        assertFalse(homeworkRepository.findById(toDelete.getId()).isPresent());
    }

}