package by.itstep.stephomework.util;

import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.entity.enums.AnswerStatus;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


public class GenerateEntityUtil {

    private static Integer nextIdForAnswerEntity = 1;

    /**
     * generate one HomeworkEntity, returned one HomeworkEntity,
     * main entity without ID!
     */
    public static HomeworkEntity generateHomeworkEntity() {
        HomeworkEntity entity = new HomeworkEntity();
        entity.setTeacherId((int) (Math.random() * 10));
        entity.setGroupId((int) (Math.random() * 10));
        entity.setCreated(Instant.now().truncatedTo(ChronoUnit.SECONDS));
        entity.setDeadLine(Instant.now().truncatedTo(ChronoUnit.SECONDS).
                plusSeconds(2592000));
        entity.setTopic("topic#" + Math.random() * 100);
        entity.setDescription("description#" + Math.random() * 100);
        entity.setTaskResourceUrl("file#" + Math.random() * 100);
        entity.setLessonDate(Instant.now().truncatedTo(ChronoUnit.SECONDS).
                minusSeconds(2592000));
        return entity;
    }

    /**
     * generate List with HomeworkEntity, returned list with HomeworkEntitys
     * all main entity`s without ID!
     */
    public static List<HomeworkEntity> generateListHomeworkEntity() {
        List<HomeworkEntity> listEntity = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            listEntity.add(generateHomeworkEntity());
        }
        return listEntity;
    }

    /**
     * generate AnswerEntity, returned one AnswerEntity,
     * to generate need parent TaskItemEntity,
     * WARNING!TaskItemEntity must by created first on DB(need parent entity ID!)
     * main entity generated without ID!
     * student_id in entity is random!
     */
    public static AnswerEntity generateAnswerEntity(HomeworkEntity parentEntity) {
        AnswerEntity entity = new AnswerEntity();
        entity.setHomework(parentEntity);
        entity.setCreated(Instant.now().truncatedTo(ChronoUnit.SECONDS));
        entity.setAnswerResourceUrl("answer#" + Math.random()*100);
        entity.setTeacherComment("teacher say#" + Math.random()*100);
        entity.setStudentId((int)(Math.random()*100));
        entity.setAnswerStatus(AnswerStatus.PENDING);
        entity.setStudentComment("student say#" + Math.random()*100);
        entity.setRealGrade((int)(Math.random()*10));
        return entity;
    }

    /**
     * generate list with AnswerEntity, returned list of AnswerEntity,
     * to generate need parent TaskItemEntity,
     * WARNING!TaskItemEntity must by created first on DB(need parent entity ID!)
     * all main entity`s generated without ID!
     * student_id in all entity`s is random!
     */
    public static List<AnswerEntity> generateListAnswerEntity(HomeworkEntity parentEntity){
        List<AnswerEntity> listEntity = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            listEntity.add(generateAnswerEntity(parentEntity));
        }
        return listEntity;
    }

    public static List<AnswerEntity> generateListAnswerEntityWithStudentId(HomeworkEntity parentEntity,
                                                                           Integer studentId){
        List<AnswerEntity> listEntity = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            AnswerEntity addedEntity = generateAnswerEntity(parentEntity);
            addedEntity.setStudentId(studentId);
            listEntity.add(addedEntity);
        }
        return listEntity;
    }

    public static List<AnswerEntity> generateListAnswerEntityWithId
            (HomeworkEntity parentEntity){
        List<AnswerEntity> listEntity = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            AnswerEntity generatedEntity = generateAnswerEntity(parentEntity);
            generatedEntity.setId(nextIdForAnswerEntity);
            listEntity.add(generatedEntity);
            nextIdForAnswerEntity++;
        }
        return listEntity;
    }

    public static AnswerEntity generateAnswerEntityForMapper(){
        HomeworkEntity grandParentEntity = generateHomeworkEntity();
        grandParentEntity.setId(1);

        return generateAnswerEntity(grandParentEntity);
    }

    public static List<AnswerEntity> generateListAnswerEntityWithId(){
       HomeworkEntity parentEntity = generateHomeworkEntity();
       parentEntity.setId(1);
       return generateListAnswerEntityWithId(parentEntity);
    }

}
