package by.itstep.stephomework.util;


import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;

import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.entity.enums.AnswerStatus;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class GenerateDtoUtil {

    private static Integer nextIdForAnswer = 1;
    private static Integer nextIdForAnswerItem = 1;
    private static Integer nextIdForHomework = 1;

    public static HomeworkCreateDto generateHomeworkCreateDto(){
        HomeworkCreateDto homeworkCreateDto = new HomeworkCreateDto();
        homeworkCreateDto.setGroupId(1);
        homeworkCreateDto.setTeacherId(1);
        homeworkCreateDto.setDeadLine(Instant.now().truncatedTo(ChronoUnit.SECONDS).
                plus(1L,ChronoUnit.DAYS));
        homeworkCreateDto.setTopic("Topic#" + Math.random()*100);
        homeworkCreateDto.setDescription("Description#" + Math.random()*100);
        homeworkCreateDto.setTaskResourceUrl("File#" + Math.random() * 100);
        homeworkCreateDto.setLessonDate(Instant.now().truncatedTo(ChronoUnit.SECONDS).
                minus(1L,ChronoUnit.DAYS));
        return homeworkCreateDto;
    }



    public static List<AnswerCreateDto> generateAnswerCreateDtoList(){
       List<AnswerCreateDto> answerCreateDtoList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
           answerCreateDtoList.add(generateAnswerCreateDto());
        }
       return answerCreateDtoList;
    }

    public static HomeworkUpdateDto generateHomeworkUpdateDto(){
       HomeworkUpdateDto homeworkUpdateDto = new HomeworkUpdateDto();
       homeworkUpdateDto.setId(nextIdForHomework);
       nextIdForHomework++;
       homeworkUpdateDto.setDeadLine(Instant.now().truncatedTo(ChronoUnit.SECONDS).
               plus(1L,ChronoUnit.DAYS));
       homeworkUpdateDto.setTopic("Topic#" + Math.random() * 100);
       homeworkUpdateDto.setDescription("Description#" + Math.random() * 100);
       homeworkUpdateDto.setTaskResourceUrl("File#" + Math.random() * 100);
       homeworkUpdateDto.setGroupId(1);
       homeworkUpdateDto.setLessonDate(Instant.now().truncatedTo(ChronoUnit.SECONDS).
               minus(1L,ChronoUnit.DAYS));
       return homeworkUpdateDto;
    }

    public static HomeworkUpdateDto generateHomeworkUpdateDtoForService(HomeworkEntity entity){
        HomeworkUpdateDto homeworkUpdateDto = new HomeworkUpdateDto();
        homeworkUpdateDto.setId(entity.getId());
        homeworkUpdateDto.setDeadLine(entity.getDeadLine().
                plus(1L,ChronoUnit.DAYS));
        homeworkUpdateDto.setTopic("Topic#" + Math.random() * 100);
        homeworkUpdateDto.setDescription("Description#" + Math.random() * 100);
        homeworkUpdateDto.setTaskResourceUrl("File#" + Math.random() * 100);
        homeworkUpdateDto.setGroupId(1);
        homeworkUpdateDto.setLessonDate(Instant.now().truncatedTo(ChronoUnit.SECONDS).
                minus(1L,ChronoUnit.DAYS));
        return homeworkUpdateDto;
    }



    public static AnswerCreateDto generateAnswerCreateDto(){
       AnswerCreateDto answerCreateDto = new AnswerCreateDto();
       answerCreateDto.setHomeworkId(nextIdForAnswer);
       answerCreateDto.setStudentId(1);
       answerCreateDto.setAnswerResourceUrl("Answear#" + Math.random() * 100);
       answerCreateDto.setStudentComment("student say#" + Math.random()*100);
       return answerCreateDto;
    }

    public static AnswerUpdateDto generateAnswerUpdateDto(){
       AnswerUpdateDto answerUpdateDto = new AnswerUpdateDto();
       answerUpdateDto.setId(nextIdForAnswerItem);
       nextIdForAnswerItem++;
        answerUpdateDto.setRealGrade((int) (Math.random() * 10));
        answerUpdateDto.setAnswerResourceUrl("Answear#" + Math.random() * 100);
        answerUpdateDto.setTeacherComment("Teacher say#" + Math.random() * 100);
        answerUpdateDto.setAnswerStatus(AnswerStatus.PENDING);
        answerUpdateDto.setStudentComment("student say#" + Math.random()*100);
       return answerUpdateDto;
    }

    public static AnswerCreateDto generateAnswerCreateDtoForService(HomeworkEntity parentHomeworkEntity){
        AnswerCreateDto answerCreateDto = new AnswerCreateDto();
        answerCreateDto.setHomeworkId(parentHomeworkEntity.getId());
        answerCreateDto.setStudentId(1);
        answerCreateDto.setAnswerResourceUrl("Answear#" + Math.random() * 100);
        answerCreateDto.setStudentComment("student say#" + Math.random()*100);
        return answerCreateDto;
    }

    public static AnswerUpdateDto generateAnswerUpdateDtoForService(AnswerEntity answerEntity){
        AnswerUpdateDto answerUpdateDto = new AnswerUpdateDto();
        answerUpdateDto.setId(answerEntity.getId());
        answerUpdateDto.setRealGrade((int) (Math.random() * 10));
        answerUpdateDto.setAnswerResourceUrl("Answear#" + Math.random() * 100);
        answerUpdateDto.setTeacherComment("Teacher say#" + Math.random() * 100);
        answerUpdateDto.setAnswerStatus(AnswerStatus.PENDING);
        answerUpdateDto.setStudentComment("student say#" + Math.random()*100);
        return answerUpdateDto;
    }

}
