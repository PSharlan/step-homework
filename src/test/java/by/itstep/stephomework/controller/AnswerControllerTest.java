package by.itstep.stephomework.controller;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.entity.enums.AnswerStatus;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AnswerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private HomeworkRepository homeworkRepository;

    @BeforeEach
    void setUp(){
        answerRepository.deleteAll();
        homeworkRepository.deleteAll();
    }

    @Test
    void getById_happyPath() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToSave = generateAnswerEntity(grandParentHomeworkEntity);
        AnswerEntity savedAnswerEntity = answerRepository.save(answerEntityToSave);
        //when
        MvcResult result = mockMvc.perform(get("/answers/{id}",
                savedAnswerEntity.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        AnswerFullDto answerFullDto = objectMapper.readValue(bytes, AnswerFullDto.class);
        //then
        assertNotNull(answerFullDto);
        assertEquals(savedAnswerEntity.getId(), answerFullDto.getId());
    }

    @Test
    void getById_whenIdNotExist() throws Exception {
        //given
        Integer notExistId = 10000;
        //when&then
        mockMvc.perform(get("/answers/{id}",
                notExistId))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void getAllByStudentId_happyPath() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        Integer studentId = 2;
        List<AnswerEntity> answersListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
            AnswerEntity setStudentAnswerEntity = answersListToSave.get(1);
            setStudentAnswerEntity.setStudentId(studentId);
            answersListToSave.set(1, setStudentAnswerEntity);
            answerRepository.saveAll(answersListToSave);

        Pageable firstPageWithThreeElements = PageRequest.of(0, 3);
        //when
        mockMvc.perform(get("/students/{studentId}/answers",
                studentId, firstPageWithThreeElements))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andReturn();
    }

    @Test
    void getByStudentAndHomewrokId_happyPath() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        Integer studentId = 2;
        List<AnswerEntity> answerEntityListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
        AnswerEntity setStudentEntity = answerEntityListToSave.get(1);
        setStudentEntity.setStudentId(studentId);
        answerEntityListToSave.set(1, setStudentEntity);
        answerRepository.saveAll(answerEntityListToSave);

        //when&then
        String url = "/answers?studentId=" + studentId + "&homeworkId="
                    + grandParentHomeworkEntity.getId();
            MvcResult result = mockMvc.perform(get(url))
                    .andExpect(status().isOk())
                    .andReturn();
            byte[] bytes = result.getResponse().getContentAsByteArray();
            AnswerFullDto[] foundAnswerFullDtos =
                    objectMapper.readValue(bytes, AnswerFullDto[].class);
        for (AnswerFullDto foundAnswerFullDto:foundAnswerFullDtos) {

            assertNotNull(foundAnswerFullDto);
            assertEquals(studentId, foundAnswerFullDto.getStudentId());
            assertEquals(grandParentHomeworkEntity.getId(), foundAnswerFullDto.getHomeworkId());
        }

    }

    @Test
    void getByStudentAndHomeworkId_whenAnswerIsNotFound() throws Exception {
        //given
        Integer notExistStudentId = 10000;
        Integer notExistHomeworkId = 10000;
        //when&then
        String url = "/answers?studentId=" + notExistStudentId + "&homeworkId="
                + notExistHomeworkId;
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        //when
        MvcResult result = mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerToSaveCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();
        byte[] bytes = result.getResponse().getContentAsByteArray();
        AnswerFullDto savedAnswerFullDto = objectMapper.readValue(bytes, AnswerFullDto.class);
        //then
        assertNotNull(savedAnswerFullDto);
        assertNotNull(savedAnswerFullDto.getId());
        assertEquals(grandParentHomeworkEntity.getId(), savedAnswerFullDto.getHomeworkId());
        assertEquals(answerToSaveCreateDto.getAnswerResourceUrl(),
                savedAnswerFullDto.getAnswerResourceUrl());
    }

    @Test
    void create_whenTooLate() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        grandParentHomeworkEntity.setDeadLine(Instant.now().truncatedTo(ChronoUnit.SECONDS)
                .minusSeconds(252900));
        homeworkRepository.save(grandParentHomeworkEntity);
        //when&then
        mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerToSaveCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenAnswerURLIsEmpty() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        answerToSaveCreateDto.setAnswerResourceUrl(null);
        //when&then
        mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerToSaveCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenStudentIdIsNull() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        answerToSaveCreateDto.setStudentId(null);
        //when&then
        mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerToSaveCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkIdIsNull() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        answerToSaveCreateDto.setHomeworkId(null);
        //when&then
        mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerToSaveCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void created_whenHomeworkNotFound() throws Exception {
        //given
        AnswerCreateDto answerCreateDto = generateAnswerCreateDto();
        Integer notExistHomeworkId = 10000;
        answerCreateDto.setHomeworkId(notExistHomeworkId);
        //when&then
        mockMvc.perform(post("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerCreateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToUpdate = generateAnswerEntity(grandParentHomeworkEntity);
        answerEntityToUpdate.setRealGrade(null);
        answerEntityToUpdate = answerRepository.save(answerEntityToUpdate);
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDtoForService(answerEntityToUpdate);
        //when
        MvcResult result = mockMvc.perform(put("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerUpdateDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = result.getResponse().getContentAsByteArray();
        AnswerFullDto updatedAnswerFullDto = objectMapper.readValue(bytes, AnswerFullDto.class);
        //then
        assertNotNull(updatedAnswerFullDto);
        assertEquals(answerEntityToUpdate.getId(), updatedAnswerFullDto.getId());
        assertNotEquals(answerEntityToUpdate.getAnswerResourceUrl(),
                updatedAnswerFullDto.getAnswerResourceUrl());
        assertEquals(answerUpdateDto.getAnswerResourceUrl(),
                updatedAnswerFullDto.getAnswerResourceUrl());
    }

    @Test
    void update_whenAnswerNotFound() throws Exception {
        //given
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDto();
        Integer notExistAnswerId = 10000;
        answerUpdateDto.setId(notExistAnswerId);
        //when&then
        mockMvc.perform(put("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerUpdateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void update_whenAnswerIdNull() throws Exception {
        //given
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDto();
        answerUpdateDto.setId(null);
        //when&then
        mockMvc.perform(put("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenAnswerURLIsEmpty() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToUpdate = answerRepository
                .save(generateAnswerEntity(grandParentHomeworkEntity));
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDtoForService(answerEntityToUpdate);
        answerUpdateDto.setAnswerResourceUrl(null);
        //when&then
        mockMvc.perform(put("/answers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answerUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void delete_happyPath() throws Exception {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());

        AnswerEntity answerEntityToDelete = answerRepository
                .save(generateAnswerEntity(grandParentHomeworkEntity));
        answerEntityToDelete.setTeacherComment(null);
        answerEntityToDelete.setRealGrade(null);
        answerEntityToDelete = answerRepository.save(answerEntityToDelete);
        //when
        mockMvc.perform(delete("/answers/{id}", answerEntityToDelete.getId()))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(delete("/answers/{id}", answerEntityToDelete.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenAnswerHasGradeOrTeacherComment() throws Exception{
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToDelete = answerRepository
                .save(generateAnswerEntity(grandParentHomeworkEntity));
        answerEntityToDelete.setAnswerStatus(AnswerStatus.ASSESSED);
        answerRepository.save(answerEntityToDelete);
        //when&then
        mockMvc.perform(delete("/answers/{id}", answerEntityToDelete.getId()))
                .andExpect(status().isBadRequest());
    }

    @Test
    void delete_whenAnswerNotExist() throws Exception{
        //given
        Integer notExistId = 10000;
        //when&then
        mockMvc.perform(delete("/answers/{id}", notExistId))
                .andExpect(status().isNotFound());
    }

}
