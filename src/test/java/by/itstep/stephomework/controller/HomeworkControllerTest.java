package by.itstep.stephomework.controller;

import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class HomeworkControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private HomeworkRepository homeworkRepository;

    @BeforeEach
    void setUp(){
        answerRepository.deleteAll();
        homeworkRepository.deleteAll();
    }

    @Test
    void getById_happyPath() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        //when
        MvcResult result = mockMvc.perform(get("/homeworks/{id}",
                homeworkEntity.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        HomeworkFullDto homeworkFullDto = objectMapper.readValue(bytes, HomeworkFullDto.class);
        //then
        assertNotNull(homeworkFullDto);
        assertEquals(homeworkEntity.getId(), homeworkFullDto.getId());
    }

    @Test
    void getById_whenHomeworkNotFound() throws Exception{
        //given
        Integer notExistId = 10000;
        //when&then
        mockMvc.perform(get("/homeworks/{id}",
                notExistId))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void getByGroupId_happyPath() throws Exception {
        //given
        Integer groupId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            homeworkRepository.save(entityToSave);
        }
        PageRequest pageRequest = PageRequest.of(0,3);
        //when
        mockMvc.perform(get("/students/groups/{groupId}/homeworks",
                groupId, pageRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andReturn();
    }

    @Test
    void getByGroupIdWithAnswers_happyPath() throws Exception {
        //given
        Integer groupId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            homeworkRepository.save(entityToSave);
        }
        List<HomeworkEntity> homeworkEntitiesWithAnswerForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesWithAnswerForSave){
            entityToSave.setGroupId(groupId);
            entityToSave = homeworkRepository.save(entityToSave);
            answerRepository.save(generateAnswerEntity(entityToSave));
        }
        PageRequest pageRequest = PageRequest.of(0,6);
        //when
        mockMvc.perform(get("/teachers/groups/{groupId}/homeworks",
                groupId, pageRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(6)))
                .andReturn();
    }

    @Test
    void getByGroupIdWithAnswersByStudentId_happyPath() throws Exception {
        //given
        Integer groupId=2;
        Integer studentId = 3;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            entityToSave = homeworkRepository.save(entityToSave);
            answerRepository.saveAll(generateListAnswerEntityWithStudentId(entityToSave, studentId));
        }
        PageRequest pageRequest = PageRequest.of(0,3);
        //when
        mockMvc.perform(get("/groups/{groupId}/students/{studentId}/homeworks",
                groupId, studentId, pageRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andReturn();
    }

    @Test
    void getTeacherId_happyPath() throws Exception {
        //given
        Integer teacherId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setTeacherId(teacherId);
            homeworkRepository.save(entityToSave);
        }
        PageRequest pageRequest = PageRequest.of(0,3);
        //when
        mockMvc.perform(get("/teachers/{teacherId}/homeworks",
                teacherId, pageRequest))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        //when
        MvcResult result = mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();
        byte[] bytes = result.getResponse().getContentAsByteArray();
        HomeworkFullDto homeworkFullDto = objectMapper.readValue(bytes, HomeworkFullDto.class);

        //then
        assertNotNull(homeworkFullDto);
        assertNotNull(homeworkFullDto.getId());
    }

    @Test
    void create_whenHomeworkTeacherIdIsNull() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setTeacherId(null);
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkDeadLineIsNull() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setDeadLine(null);
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkGroupIdIsNull() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setGroupId(null);
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkTopicIsEmpty() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setTopic(null);
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkDescriptionIsEmpty() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setDescription(null);
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_whenHomeworkDeadLineIsBeforeCreated() throws Exception{
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setDeadLine(Instant.now().minus(2L, ChronoUnit.DAYS).
                truncatedTo(ChronoUnit.SECONDS));
        //when&then
        mockMvc.perform(post("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkCreateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_happyPath() throws Exception{
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        //when
        MvcResult result = mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] bytes = result.getResponse().getContentAsByteArray();
        HomeworkFullDto homeworkFullDto = objectMapper.readValue(bytes, HomeworkFullDto.class);
        //then
        assertNotNull(homeworkFullDto);
        assertEquals(homeworkEntity.getId(),homeworkFullDto.getId());
        assertNotEquals(homeworkEntity.getDeadLine(),homeworkFullDto.getDeadLine());
        assertNotEquals(homeworkEntity.getTopic(),homeworkFullDto.getTopic());
    }

    @Test
    void update_whenHomeworkIdIsNull() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setId(null);
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenHomeworkDeadLineIsNull() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setDeadLine(null);
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenHomeworkLessonDateIsAfterDeadLineOrCreated() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setLessonDate(Instant.now().plus(2L,ChronoUnit.DAYS).
                truncatedTo(ChronoUnit.SECONDS));
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenHomeworkGroupIdIsNull() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setGroupId(null);
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenHomeworkTopicIsEmpty() throws Exception {
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setTopic(null);
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void update_whenHomeworkNotFound() throws Exception{
        //given
        Integer notExistId = 10000;
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDto();
        homeworkUpdateDto.setId(notExistId);
        //when&then
        mockMvc.perform(put("/homeworks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(homeworkUpdateDto)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void delete_happyPath() throws Exception{
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        //when
        mockMvc.perform(delete("/homeworks/{id}", homeworkEntity.getId()))
                .andExpect(status().isOk());
        //then
        mockMvc.perform(delete("/homeworks/{id}", homeworkEntity.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenHomeworkHasTask() throws Exception{
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        answerRepository.save(generateAnswerEntity(homeworkEntity));
        //when&then
        mockMvc.perform(delete("/homeworks/{id}", homeworkEntity.getId()))
                .andExpect(status().isBadRequest());
    }

    @Test
    void delete_whenHomeworkNotExist() throws Exception{
        //given
        Integer notExistId = 10000;
        //when&then
        mockMvc.perform(delete("/homeworks/{id}", notExistId))
                .andExpect(status().isNotFound());
    }

}
