package by.itstep.stephomework.service;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.entity.enums.AnswerStatus;
import by.itstep.stephomework.exception.*;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class AnswerServiceTest {

    @Autowired
    private AnswerService answerService;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private HomeworkRepository homeworkRepository;

    @BeforeEach
    void setUp(){
        answerRepository.deleteAll();
        homeworkRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToSave = generateAnswerEntity(grandParentHomeworkEntity);
        AnswerEntity savedAnswerEntity = answerRepository.save(answerEntityToSave);
        //when
        AnswerFullDto foundAnswerFullDto = answerService.findById(savedAnswerEntity.getId());
        //then
        assertNotNull(foundAnswerFullDto);
        assertEquals(savedAnswerEntity.getId(), foundAnswerFullDto.getId());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistId = 10000;
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> answerService.findById(notExistId));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistId)));
    }

    @Test
    void findAllByStudentId_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        Integer studentId = 2;

            List<AnswerEntity> answersListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
            AnswerEntity setStudentAnswerEntity = answersListToSave.get(1);
            setStudentAnswerEntity.setStudentId(studentId);
            answersListToSave.set(1, setStudentAnswerEntity);
            answerRepository.saveAll(answersListToSave);

        Pageable pageable = Pageable.unpaged();
        //when
        Page<AnswerFullDto> answerFullDtoList = answerService.findAllByStudentId(studentId, pageable);
        //then
        assertNotNull(answerFullDtoList);
        for (AnswerFullDto answerFullDto : answerFullDtoList) {
            assertEquals(studentId, answerFullDto.getStudentId());
        }
    }

    @Test
    void findByStudentIdAndHomeworkId_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        Integer studentId = 2;

        List<AnswerEntity> answerEntityListToSave = generateListAnswerEntity(grandParentHomeworkEntity);
        AnswerEntity setStudentEntity = answerEntityListToSave.get(1);
        setStudentEntity.setStudentId(studentId);
        answerEntityListToSave.set(1, setStudentEntity);
        answerRepository.saveAll(answerEntityListToSave);

        //when
        List<AnswerFullDto> foundAnswerFullDtoList =
                    answerService.findByStudentIdAndHomeworkId(studentId, grandParentHomeworkEntity.getId());
        //then
        for(AnswerFullDto foundAnswerFullDto:foundAnswerFullDtoList) {
            assertNotNull(foundAnswerFullDto);
            assertEquals(studentId, foundAnswerFullDto.getStudentId());
            assertEquals(grandParentHomeworkEntity.getId(), foundAnswerFullDto.getHomeworkId());
        }
    }

    @Test
    void created_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        //when
        AnswerFullDto answerFullDtoCreated = answerService.create(answerToSaveCreateDto);
        //then
        assertNotNull(answerFullDtoCreated);
        assertNotNull(answerFullDtoCreated.getId());
    }

    @Test
    void created_whenHomeworkNotFound() {
        //given
        AnswerCreateDto answerCreateDto = generateAnswerCreateDto();
        Integer notExistHomeworkId = 10000;
        answerCreateDto.setHomeworkId(notExistHomeworkId);
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> answerService.create(answerCreateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistHomeworkId)));
    }

    @Test
    void created_whenTooLate() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        grandParentHomeworkEntity.setDeadLine(Instant.now().truncatedTo(ChronoUnit.SECONDS)
                .minusSeconds(252900));
        grandParentHomeworkEntity = homeworkRepository.save(grandParentHomeworkEntity);
        //when
        Exception exception = assertThrows(AnswerTooLateException.class,
                () -> answerService.create(answerToSaveCreateDto));
        //then
        assertTrue(exception.getMessage().contains(String
                .valueOf(grandParentHomeworkEntity.getDeadLine())));
    }

    @Test
    void created_whenAnswerStatusRETAKE_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntity = answerRepository.save(generateAnswerEntity(grandParentHomeworkEntity));
        answerEntity.setAnswerStatus(AnswerStatus.RETAKE);
        answerRepository.save(answerEntity);

        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        answerToSaveCreateDto.setStudentId(answerEntity.getStudentId());
        //when
        AnswerFullDto answerFullDtoCreated = answerService.create(answerToSaveCreateDto);
        //then
        assertNotNull(answerFullDtoCreated);
        assertNotNull(answerFullDtoCreated.getId());
        assertEquals(grandParentHomeworkEntity.getId(), answerFullDtoCreated.getHomeworkId());
    }

    @Test
    void created_whenAnswerStatusNotRETAKE() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerCreateDto answerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        AnswerFullDto answerFullDtoCreated = answerService.create(answerToSaveCreateDto);
        AnswerCreateDto newAnswerToSaveCreateDto = generateAnswerCreateDtoForService(grandParentHomeworkEntity);
        newAnswerToSaveCreateDto.setStudentId(answerFullDtoCreated.getStudentId());
        //when
        Exception exception = assertThrows(AnswerAlreadyExist.class,
                () -> answerService.create(newAnswerToSaveCreateDto));
        //then
        assertTrue(exception.getMessage().contains(String
                .valueOf(grandParentHomeworkEntity.getId())));
    }

    @Test
    void update_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToUpdate = generateAnswerEntity(grandParentHomeworkEntity);
        answerEntityToUpdate.setRealGrade(null);
        answerEntityToUpdate = answerRepository.save(answerEntityToUpdate);
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDtoForService(answerEntityToUpdate);

        //when
        AnswerFullDto answerFullDtoUpdated = answerService.update(answerUpdateDto);
        //then
        assertNotNull(answerFullDtoUpdated);
        assertEquals(answerEntityToUpdate.getId(), answerFullDtoUpdated.getId());
        assertNotEquals(answerEntityToUpdate.getAnswerResourceUrl(),
                answerFullDtoUpdated.getAnswerResourceUrl());
        assertEquals(answerUpdateDto.getAnswerResourceUrl(),
                answerFullDtoUpdated.getAnswerResourceUrl());
    }

    @Test
    void update_whenAnswerNotFound() {
        //given
        AnswerUpdateDto answerUpdateDto = generateAnswerUpdateDto();
        Integer notExistAnswerId = 10000;
        answerUpdateDto.setId(notExistAnswerId);
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> answerService.update(answerUpdateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistAnswerId)));
    }

    @Test
    void deleteById_happyPath() {
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToDelete = answerRepository
                .save(generateAnswerEntity(grandParentHomeworkEntity));
        answerEntityToDelete.setTeacherComment(null);
        answerEntityToDelete.setRealGrade(null);
        answerEntityToDelete = answerRepository.save(answerEntityToDelete);
        //when
        answerService.deleteById(answerEntityToDelete.getId());
        //then
        assertFalse(answerRepository.findById(answerEntityToDelete.getId()).isPresent());
    }

    @Test
    void deleteById_whenAnswerHasGradeOrTeacherComment(){
        //given
        HomeworkEntity grandParentHomeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        AnswerEntity answerEntityToDelete = generateAnswerEntity(grandParentHomeworkEntity);
        answerEntityToDelete.setAnswerStatus(AnswerStatus.ASSESSED);
        answerRepository.save(answerEntityToDelete);
        //when
        Exception exception = assertThrows(AnswerHasRatingOrCommentException.class,
                () -> answerService.deleteById(answerEntityToDelete.getId()));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(answerEntityToDelete.getId())));
    }

}
