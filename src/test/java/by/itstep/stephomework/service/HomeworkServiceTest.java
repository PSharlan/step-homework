package by.itstep.stephomework.service;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.entity.HomeworkEntity;
import by.itstep.stephomework.exception.EntityNotFoundException;
import by.itstep.stephomework.exception.HomeworkHasAnswerException;
import by.itstep.stephomework.exception.WrongDateException;
import by.itstep.stephomework.repository.AnswerRepository;
import by.itstep.stephomework.repository.HomeworkRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class HomeworkServiceTest {

    @Autowired
    private HomeworkService homeworkService;

    @Autowired
    private HomeworkRepository homeworkRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @BeforeEach
    void setUp(){
        answerRepository.deleteAll();
        homeworkRepository.deleteAll();
    }

    @Test
    void findById_happyPath(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        //when
        HomeworkFullDto homeworkFullDto = homeworkService.findById(homeworkEntity.getId());
        //then
        assertNotNull(homeworkFullDto);
        assertEquals(homeworkEntity.getId(), homeworkFullDto.getId());
    }

    @Test
    void findById_whenNotExist(){
        //given
        Integer notExistId = 10000;
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> homeworkService.findById(notExistId));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistId)));
    }

    @Test
    void findByGroupId_happyPath(){
        //given
        Integer groupId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            homeworkRepository.save(entityToSave);
        }
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkPreviewDto> homeworkPreviewDtoPages = homeworkService.findByGroupId(groupId,
                pageable);
        //then
        assertNotNull(homeworkPreviewDtoPages);
        assertEquals(homeworkEntitiesForSave.size(), homeworkPreviewDtoPages.getTotalElements());
        List<HomeworkPreviewDto> foundHomeworkPreviewDtoList = homeworkPreviewDtoPages.toList();
        foundHomeworkPreviewDtoList.forEach(homeworkPreviewDto -> assertEquals(groupId,
                homeworkPreviewDto.getGroupId()));
    }

    @Test
    void findByGroupIdWithAnswers_happyPath(){
        //given
        Integer groupId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            homeworkRepository.save(entityToSave);
        }
        List<HomeworkEntity> homeworkEntitiesWithAnswerForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesWithAnswerForSave){
            entityToSave.setGroupId(groupId);
            entityToSave = homeworkRepository.save(entityToSave);
            answerRepository.save(generateAnswerEntity(entityToSave));
        }
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkFullDto> homeworkFullDtoPages = homeworkService.findByGroupIdWithAnswers(groupId,
                pageable);
        //then
        assertNotNull(homeworkFullDtoPages);
        Long totalEntities = (long)homeworkEntitiesWithAnswerForSave.size() + homeworkEntitiesForSave.size();
        assertEquals(totalEntities, homeworkFullDtoPages.getTotalElements());
        List<HomeworkFullDto> foundHomeworkFullDtoList = homeworkFullDtoPages.toList();
        foundHomeworkFullDtoList.forEach(homeworkFullDto -> assertEquals(groupId,
                homeworkFullDto.getGroupId()));
    }

    @Test
    void findByGroupIdWithAnswersByStudentId_happyPath(){
        //given
        Integer groupId=2;
        Integer studentId = 3;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setGroupId(groupId);
            entityToSave = homeworkRepository.save(entityToSave);
            answerRepository.saveAll(generateListAnswerEntityWithStudentId(entityToSave, studentId));
        }
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkWithAnswerPreviewDTO> homeworkWithAnswerPreviewDtoPages = homeworkService
                .findByGroupIdWithAnswersByStudentId(groupId, studentId, pageable);
        //then
        assertNotNull(homeworkWithAnswerPreviewDtoPages);
        assertEquals(homeworkEntitiesForSave.size(), homeworkWithAnswerPreviewDtoPages.getTotalElements());
        List<HomeworkWithAnswerPreviewDTO> homeworkWithAnswerPreviewDTOS = homeworkWithAnswerPreviewDtoPages.toList();
        homeworkWithAnswerPreviewDTOS.forEach(homeworkPreviewDto -> assertEquals(groupId,
                homeworkPreviewDto.getGroupId()));
        homeworkWithAnswerPreviewDTOS.forEach(homeworkPreviewDto -> assertNotNull(homeworkPreviewDto.getRealGrade()));
        homeworkWithAnswerPreviewDTOS.forEach(homeworkPreviewDto -> assertNotNull(homeworkPreviewDto.getAnswerStatus()));
    }

    @Test
    void findByTeacherId_happyPath(){
        //given
        Integer teacherId=2;
        List<HomeworkEntity> homeworkEntitiesForSave = generateListHomeworkEntity();
        for(HomeworkEntity entityToSave: homeworkEntitiesForSave){
            entityToSave.setTeacherId(teacherId);
            homeworkRepository.save(entityToSave);
        }
        Pageable pageable = Pageable.unpaged();
        //when
        Page<HomeworkPreviewDto> homeworkPreviewDtoPages = homeworkService.findByTeacherId(teacherId,
                pageable);
        //then
        assertNotNull(homeworkPreviewDtoPages);
        assertEquals(homeworkEntitiesForSave.size(), homeworkPreviewDtoPages.getTotalElements());
        List<HomeworkPreviewDto> foundHomeworkPreviewDtoList = homeworkPreviewDtoPages.toList();
        foundHomeworkPreviewDtoList.forEach(homeworkPreviewDto -> assertEquals(teacherId,
                homeworkPreviewDto.getTeacherId()));
    }

    @Test
    void create_happyPath(){
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        //when
        HomeworkFullDto homeworkFullDto = homeworkService.create(homeworkCreateDto);
        //then
        assertNotNull(homeworkFullDto);
        assertNotNull(homeworkFullDto.getId());
    }

    @Test
    void create_whenDeadLineAfterCreated(){
        //given
        HomeworkCreateDto homeworkCreateDto = generateHomeworkCreateDto();
        homeworkCreateDto.setDeadLine(Instant.now().minus(2L, ChronoUnit.DAYS).
                truncatedTo(ChronoUnit.SECONDS));
        //when
        Exception exception = assertThrows(WrongDateException.class,
                () -> homeworkService.create(homeworkCreateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(
                "Dead Line must be after created date, or lesson date")));
    }

    @Test
    void update_happyPath(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        //when
        HomeworkFullDto homeworkFullDto = homeworkService.update(homeworkUpdateDto);
        //then
        assertNotNull(homeworkFullDto);
        assertEquals(homeworkEntity.getId(),homeworkFullDto.getId());
        assertNotEquals(homeworkEntity.getDeadLine(),homeworkFullDto.getDeadLine());
        assertNotEquals(homeworkEntity.getTopic(),homeworkFullDto.getTopic());
        assertNotEquals(homeworkEntity.getDescription(),homeworkFullDto.getDescription());
    }

    @Test
    void update_whenHomeworkHasAnAnswer(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        homeworkEntity.setAnswers(answerRepository.saveAll(generateListAnswerEntity(homeworkEntity)));
        homeworkRepository.save(homeworkEntity);
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);

        //when
        Exception exception = assertThrows(HomeworkHasAnswerException.class,
                () -> homeworkService.update(homeworkUpdateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf("Homework has answer(s)!")));
    }

    @Test
    void update_whenLessonDateIsAfterCreated(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDtoForService(homeworkEntity);
        homeworkUpdateDto.setLessonDate(Instant.now().plus(2L,ChronoUnit.DAYS).
                truncatedTo(ChronoUnit.SECONDS));

        //when
        Exception exception = assertThrows(WrongDateException.class,
                () -> homeworkService.update(homeworkUpdateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(
                "Lesson date must be before created date and dead line")));
    }

    @Test
    void update_whenHomeworkNotFound(){
        //given
        Integer notExistId = 10000;
        HomeworkUpdateDto homeworkUpdateDto = generateHomeworkUpdateDto();
        homeworkUpdateDto.setId(notExistId);
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> homeworkService.update(homeworkUpdateDto));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistId)));
    }

    @Test
    void deleteById_happyPath(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());
        //when
        homeworkService.deleteById(homeworkEntity.getId());
        //then
        assertFalse(homeworkRepository.findById(homeworkEntity.getId()).isPresent());
    }

    @Test
    void delete_whenHomeworkNotFound(){
        //given
        Integer notExistId = 10000;
        //when
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> homeworkService.deleteById(notExistId));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistId)));
    }

    @Test
    void delete_whenHomeworkHasAnswers(){
        //given
        HomeworkEntity homeworkEntity = homeworkRepository.save(generateHomeworkEntity());

        answerRepository.save(generateAnswerEntity(homeworkEntity));
        //when
        Exception exception = assertThrows(HomeworkHasAnswerException.class,
                () -> homeworkService.deleteById(homeworkEntity.getId()));
        //then
        assertTrue(exception.getMessage().contains("Homework has answer(s)!"));
    }

}
