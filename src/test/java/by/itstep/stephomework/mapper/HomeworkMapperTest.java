package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.homework.HomeworkCreateDto;
import by.itstep.stephomework.dto.homework.HomeworkFullDto;
import by.itstep.stephomework.dto.homework.HomeworkPreviewDto;
import by.itstep.stephomework.dto.homework.HomeworkUpdateDto;
import by.itstep.stephomework.entity.HomeworkEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.stephomework.mapper.HomeworkMapper.HOMEWORK_MAPPER;
import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.generateHomeworkEntity;

import static by.itstep.stephomework.util.GenerateEntityUtil.generateListAnswerEntity;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class HomeworkMapperTest {

    @Test
    void givenHomeworkEntityToPreviewDto_whenMaps_thenCorrect(){
        //given
        HomeworkEntity generateHomeworkEntity = generateHomeworkEntity();
        generateHomeworkEntity.setId(1);
        //when
        HomeworkPreviewDto mappingHomeworkPreviewDto =
                HOMEWORK_MAPPER.mapToPreviewDto(generateHomeworkEntity);
        //then
        assertNotNull(mappingHomeworkPreviewDto);
        assertEquals(generateHomeworkEntity.getId(), mappingHomeworkPreviewDto.getId());
        assertEquals(generateHomeworkEntity.getTeacherId(), mappingHomeworkPreviewDto.getTeacherId());
        assertEquals(generateHomeworkEntity.getTopic(), mappingHomeworkPreviewDto.getTopic());
        assertEquals(generateHomeworkEntity.getDescription(), mappingHomeworkPreviewDto.getDescription());
        assertEquals(generateHomeworkEntity.getGroupId(), mappingHomeworkPreviewDto.getGroupId());
        assertEquals(generateHomeworkEntity.getCreated(), mappingHomeworkPreviewDto.getCreated());
        assertEquals(generateHomeworkEntity.getDeadLine(), mappingHomeworkPreviewDto.getDeadLine());
        assertEquals(generateHomeworkEntity.getTaskResourceUrl(), mappingHomeworkPreviewDto.getTaskResourceUrl());
    }

    @Test
    void givenHomeworkEntityToFullDto_whenMaps_thenCorrect(){
        //given
        HomeworkEntity generateHomeworkEntity = generateHomeworkEntity();
        generateHomeworkEntity.setId(1);
        generateHomeworkEntity.setAnswers(generateListAnswerEntity
                (generateHomeworkEntity));
        //when
        HomeworkFullDto mappingHomeworkFullDto =
                HOMEWORK_MAPPER.mapToFullDto(generateHomeworkEntity);
        //then
        assertNotNull(mappingHomeworkFullDto);
        assertEquals(generateHomeworkEntity.getId(), mappingHomeworkFullDto.getId());
        assertEquals(generateHomeworkEntity.getCreated(), mappingHomeworkFullDto.getCreated());
        assertEquals(generateHomeworkEntity.getTopic(), mappingHomeworkFullDto.getTopic());
        assertEquals(generateHomeworkEntity.getDescription(), mappingHomeworkFullDto.getDescription());
        assertEquals(generateHomeworkEntity.getDeadLine(), mappingHomeworkFullDto.getDeadLine());
        assertEquals(generateHomeworkEntity.getGroupId(), mappingHomeworkFullDto.getGroupId());
        assertEquals(generateHomeworkEntity.getTaskResourceUrl(), mappingHomeworkFullDto.getTaskResourceUrl());
        assertNotNull(mappingHomeworkFullDto.getAnswers());
        List<AnswerFullDto> mappingAnswerFullDtoList = mappingHomeworkFullDto.getAnswers();
        for (AnswerFullDto mappingAnswerFullDto : mappingAnswerFullDtoList){
            assertNotNull(mappingHomeworkFullDto.getAnswers());
            assertEquals(3, mappingHomeworkFullDto.getAnswers().size());
        }
    }

    @Test
    void givenHomeworkCreateDtoToEntity_whenMaps_thenCorrect(){
        //given
        HomeworkCreateDto generatedHomeworkCreateDto = generateHomeworkCreateDto();
        //when
        HomeworkEntity mappingHomeworkEntity =
                HOMEWORK_MAPPER.mapToEntity(generatedHomeworkCreateDto);
        //then
        assertNotNull(mappingHomeworkEntity);
        assertNull(mappingHomeworkEntity.getId());
        assertNull(mappingHomeworkEntity.getCreated());
        assertEquals(generatedHomeworkCreateDto.getGroupId(), mappingHomeworkEntity.getGroupId());
        assertEquals(generatedHomeworkCreateDto.getTeacherId(), mappingHomeworkEntity.getTeacherId());
        assertEquals(generatedHomeworkCreateDto.getTopic(), mappingHomeworkEntity.getTopic());
        assertEquals(generatedHomeworkCreateDto.getDescription(), mappingHomeworkEntity.getDescription());
        assertEquals(generatedHomeworkCreateDto.getDeadLine(), mappingHomeworkEntity.getDeadLine());
        assertEquals(generatedHomeworkCreateDto.getTaskResourceUrl(), mappingHomeworkEntity.getTaskResourceUrl());
    }

    @Test
    void givenHomeworkUpdateDtoToEntity_whenMaps_thenCorrect(){
        //given
        HomeworkUpdateDto generatedHomeworkUpdateDto = generateHomeworkUpdateDto();
        //when
        HomeworkEntity mappingHomeworkEntity =
                HOMEWORK_MAPPER.mapToEntity(generatedHomeworkUpdateDto);
        //then
        assertNotNull(mappingHomeworkEntity);
        assertEquals(generatedHomeworkUpdateDto.getId(), mappingHomeworkEntity.getId());
        assertEquals(generatedHomeworkUpdateDto.getGroupId(), mappingHomeworkEntity.getGroupId());
        assertEquals(generatedHomeworkUpdateDto.getDeadLine(), mappingHomeworkEntity.getDeadLine());
        assertEquals(generatedHomeworkUpdateDto.getTopic(), mappingHomeworkEntity.getTopic());
        assertEquals(generatedHomeworkUpdateDto.getDescription(), mappingHomeworkEntity.getDescription());
        assertEquals(generatedHomeworkUpdateDto.getTaskResourceUrl(), mappingHomeworkEntity.getTaskResourceUrl());
    }

}
