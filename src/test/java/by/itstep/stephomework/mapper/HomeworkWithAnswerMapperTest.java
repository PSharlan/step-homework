package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.HomeworkWithAnswerPreviewDTO;
import by.itstep.stephomework.entity.AnswerEntity;
import by.itstep.stephomework.entity.HomeworkEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.stephomework.util.GenerateEntityUtil.generateHomeworkEntity;
import static by.itstep.stephomework.util.GenerateEntityUtil.generateListAnswerEntityWithId;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class HomeworkWithAnswerMapperTest {

    private final HomeworkWithAnswerMapper homeworkWithAnswerMapper ;

    @Autowired
    HomeworkWithAnswerMapperTest(HomeworkWithAnswerMapper homeworkWithAnswerMapper) {
        this.homeworkWithAnswerMapper = homeworkWithAnswerMapper;
    }

    @Test
    void givenHomeworkEntityToHomeworkPreviewWithAnswerDto_whenMaps_thenCorrect() {
        //given
        HomeworkEntity generateHomeworkEntity = generateHomeworkEntity();
        List<AnswerEntity> generatedAnswerEntityList = generateListAnswerEntityWithId();
        generatedAnswerEntityList.forEach(answerEntity -> answerEntity.setHomework(generateHomeworkEntity));
        Integer studentId = 2;
        generatedAnswerEntityList.forEach(answerEntity -> answerEntity.setStudentId(studentId));
        generateHomeworkEntity.setAnswers(generatedAnswerEntityList);
        AnswerEntity lastAnswer = generateHomeworkEntity.getAnswers().get(generateHomeworkEntity.getAnswers().size()-1);
        //when
        HomeworkWithAnswerPreviewDTO mappingDto = homeworkWithAnswerMapper.mapToPreview(generateHomeworkEntity,
                studentId);
        //then
        assertEquals(generateHomeworkEntity.getId(),mappingDto.getId());
        assertEquals(generateHomeworkEntity.getTopic(), mappingDto.getTopic());
        assertEquals(generateHomeworkEntity.getCreated(), mappingDto.getCreated());
        assertEquals(generateHomeworkEntity.getGroupId(), mappingDto.getGroupId());
        assertEquals(generateHomeworkEntity.getDeadLine(), mappingDto.getDeadLine());
        assertEquals(generateHomeworkEntity.getTeacherId(), mappingDto.getTeacherId());
        assertEquals(generateHomeworkEntity.getDescription(), mappingDto.getDescription());
        assertEquals(generateHomeworkEntity.getTaskResourceUrl(), mappingDto.getTaskResourceUrl());
        assertEquals(generateHomeworkEntity.getLessonDate(), mappingDto.getLessonDate());
        assertEquals(lastAnswer.getRealGrade(), mappingDto.getRealGrade());
        assertEquals(lastAnswer.getAnswerStatus(), mappingDto.getAnswerStatus());
    }
}