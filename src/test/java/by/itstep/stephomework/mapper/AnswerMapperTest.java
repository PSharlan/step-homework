package by.itstep.stephomework.mapper;

import by.itstep.stephomework.dto.answer.AnswerCreateDto;
import by.itstep.stephomework.dto.answer.AnswerFullDto;
import by.itstep.stephomework.dto.answer.AnswerUpdateDto;
import by.itstep.stephomework.entity.AnswerEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.stephomework.mapper.AnswerMapper.ANSWER_MAPPER;
import static by.itstep.stephomework.util.GenerateDtoUtil.*;
import static by.itstep.stephomework.util.GenerateEntityUtil.generateAnswerEntityForMapper;
import static by.itstep.stephomework.util.GenerateEntityUtil.generateListAnswerEntityWithId;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class AnswerMapperTest {

    @Test
    void givenAnswerEntityToFullDto_whenMaps_thenCorrect(){
        //given
        AnswerEntity generatedAnswerEntity = generateAnswerEntityForMapper();
        //when
        AnswerFullDto mappingAnswerFullDto = ANSWER_MAPPER.mapToFullDto(generatedAnswerEntity);
        //then
        assertNotNull(mappingAnswerFullDto);
        assertEquals(generatedAnswerEntity.getId(), mappingAnswerFullDto.getId());
        assertEquals(generatedAnswerEntity.getRealGrade(), mappingAnswerFullDto.getRealGrade());
        assertEquals(generatedAnswerEntity.getCreated(), mappingAnswerFullDto.getCreated());
        assertEquals(generatedAnswerEntity.getAnswerResourceUrl(),
                mappingAnswerFullDto.getAnswerResourceUrl());
        assertEquals(generatedAnswerEntity.getStudentId(), mappingAnswerFullDto.getStudentId());
        assertEquals(generatedAnswerEntity.getTeacherComment(),
                mappingAnswerFullDto.getTeacherComment());
        assertEquals(generatedAnswerEntity.getStudentComment(), mappingAnswerFullDto.getStudentComment());
        assertNotNull(mappingAnswerFullDto.getHomeworkId());
        assertEquals(generatedAnswerEntity.getHomework().getId(),
                mappingAnswerFullDto.getHomeworkId());
    }

    @Test
    void givenAnswerEntityListToFullDtoList_whenMaps_thenCorrect() {
        //given
        List<AnswerEntity> generatedAnswerEntityList = generateListAnswerEntityWithId();
        //when
        List<AnswerFullDto> mappingAnswerFullDtoList =
                ANSWER_MAPPER.mapToFullDtoList(generatedAnswerEntityList);
        //then
        assertNotNull(mappingAnswerFullDtoList);
        assertEquals(generatedAnswerEntityList.size(), mappingAnswerFullDtoList.size());
        for (int i = 0; i < mappingAnswerFullDtoList.size(); i++) {
            assertNotNull(mappingAnswerFullDtoList.get(i));
            assertEquals(generatedAnswerEntityList.get(i).getId(),
                    mappingAnswerFullDtoList.get(i).getId());
            assertEquals(generatedAnswerEntityList.get(i).getRealGrade(),
                    mappingAnswerFullDtoList.get(i).getRealGrade());
            assertEquals(generatedAnswerEntityList.get(i).getCreated(),
                    mappingAnswerFullDtoList.get(i).getCreated());
            assertEquals(generatedAnswerEntityList.get(i).getAnswerResourceUrl(),
                    mappingAnswerFullDtoList.get(i).getAnswerResourceUrl());
            assertEquals(generatedAnswerEntityList.get(i).getStudentId(),
                    mappingAnswerFullDtoList.get(i).getStudentId());
            assertEquals(generatedAnswerEntityList.get(i).getTeacherComment(),
                    mappingAnswerFullDtoList.get(i).getTeacherComment());
            assertNotNull(mappingAnswerFullDtoList.get(i).getHomeworkId());
            assertEquals(generatedAnswerEntityList.get(i).getHomework().getId(),
                    mappingAnswerFullDtoList.get(i).getHomeworkId());
            assertEquals(generatedAnswerEntityList.get(i).getStudentComment(),
                    mappingAnswerFullDtoList.get(i).getStudentComment());
        }
    }

    @Test
    void givenAnswerCreateDtoToEntity_whenMaps_thenCorrect(){
        //given
        AnswerCreateDto generatedAnswerCreateDto = generateAnswerCreateDto();
        //when
        AnswerEntity mappingAnswerEntity = ANSWER_MAPPER.mapToEntity(generatedAnswerCreateDto);
        //then
        assertNotNull(mappingAnswerEntity);
        assertNull(mappingAnswerEntity.getId());
        assertNotNull(mappingAnswerEntity.getHomework());
        assertEquals(generatedAnswerCreateDto.getHomeworkId(),
                mappingAnswerEntity.getHomework().getId());
        assertEquals(generatedAnswerCreateDto.getStudentId(), mappingAnswerEntity.getStudentId());
        assertEquals(generatedAnswerCreateDto.getAnswerResourceUrl(),
                mappingAnswerEntity.getAnswerResourceUrl());
        assertEquals(generatedAnswerCreateDto.getStudentComment(),
                mappingAnswerEntity.getStudentComment());
    }

    @Test
    void givenAnswerUpdateDtoToEntity_whenMaps_thenCorrect() {
        //given
        AnswerUpdateDto generatedAnswerUpdateDto = generateAnswerUpdateDto();
        //when
        AnswerEntity mappingAnswerEntity =
                ANSWER_MAPPER.mapToEntity(generatedAnswerUpdateDto);
        //then
        assertNotNull(mappingAnswerEntity);
        assertEquals(generatedAnswerUpdateDto.getId(), mappingAnswerEntity.getId());
        assertEquals(generatedAnswerUpdateDto.getRealGrade(), mappingAnswerEntity.getRealGrade());
        assertEquals(generatedAnswerUpdateDto.getAnswerResourceUrl(),
                mappingAnswerEntity.getAnswerResourceUrl());
        assertEquals(generatedAnswerUpdateDto.getTeacherComment(),
                mappingAnswerEntity.getTeacherComment());
        assertEquals(generatedAnswerUpdateDto.getStudentComment(),
                mappingAnswerEntity.getStudentComment());
    }

}
