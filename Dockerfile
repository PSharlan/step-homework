FROM openjdk:11

ADD target/*.jar /app_step-homework.jar

ENTRYPOINT ["java", "-jar", "app_step-homework.jar"]
